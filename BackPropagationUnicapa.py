# -*- coding: utf-8 -*-

import random
import math
import pickle
from pylab import ylim, show, plot, ion


class BackPropagationUnicapa:
    def __init__(self, cantidad_entradas, cantidad_salidas):
        self.m = cantidad_entradas
        self.n = cantidad_salidas
        self.p = None # Cantidad de patrones
        self.funcion_de_activacion = None
        self.errores = []
        self.inicializar_pesos()
        self.inicializar_umbrales()
        self.establecer_parametros_entrenamiento_iniciales()

    def inicializar_pesos(self):
        # w[neurona_salida, neurona_entrada]
        self.w = [[random.uniform(-1.0, 1.0) for i in range(self.n)] for j in range(self.m)]

    def inicializar_umbrales(self):
        # u[neurona_salida]
        self.u = [random.uniform(-1.0, 1.0) for i in range(self.n)]

    def configurar_red_antes_de_entrenamiento(self):
        self.y = [[0.0 for p in range(self.p)] for i in range(self.n)] # y[[salida 1], [salida 2], ...]
        self.el = [[0.0 for i in range(self.n)] for p in range(self.p)] # el[[errores patron 1], [errores patron 2], ...]
        self.enl = [[0.0 for i in range(self.n)] for p in range(self.p)] # el[[errores patron 1], [errores patron 2], ...]
        self.ep = [0.0 for p in range(self.p)] # ep[errores...]
        self.rms = 0.0

    def establecer_parametros_entrenamiento_iniciales(self):
        self.trainParam = {}
        self.trainParam['epochs'] = 1000
        self.trainParam['goal'] = 0.01
        self.trainParam['lr'] = 0.01

    def establecer_cantidad_de_patrones(self, cantidad_patrones = None):
        self.p = len(self.x) if cantidad_patrones == None else cantidad_patrones

    def calcular_salida_de_la_red(self, patron):
        for i in range(self.n):
            resultado = 0
            for j in range(self.m):
                resultado += self.x[patron][j] * self.w[j][i]
            resultado -= self.u[i]
            self.y[i][patron] = self.aplicar_funcion_de_activacion(resultado)

    def calcular_errores_lineales_del_patron(self, patron):
        self.el[patron] = [(self.yd[i][patron] - self.y[i][patron]) for i in range(self.n)]

    def calcular_errores_no_lineales_del_patron(self, patron):
        for i in range(self.n):
            resultado = 0
            for j in range(self.m):
                resultado += self.el[patron][i] * self.w[j][i]
            self.enl[patron][i] = resultado

    def calcular_error_del_patron(self, patron):
        self.ep[patron] = sum([abs(err) for err in self.el[patron]]) / self.n

    def aplicar_funcion_de_activacion(self, value):
        if self.funcion_de_activacion == 'tanh':
            return math.tanh(value)
        elif self.funcion_de_activacion == 'sigmoid':
            return 1 / (1 + math.exp(-value))

    def derivada_funcion_de_activacion(self, value):
        try:
            if self.funcion_de_activacion == 'tanh':
                return 1 / math.pow(math.cosh(value), 2) # 1 / (cosh(x))^2
            elif self.funcion_de_activacion == 'sigmoid':
                return math.exp(value) / math.pow(math.exp(value) + 1, 2) # e^t / (e^t + 1)^2
        except:
            return 0

    def actualzar_pesos_y_umbrales(self, patron):
        for i in range(self.n):
            for j in range(self.m):
                self.w[j][i] += self.trainParam['lr'] * self.enl[patron][i] * self.derivada_funcion_de_activacion(self.y[i][patron]) * self.x[patron][j]
            self.u[i] += self.trainParam['lr'] * self.enl[patron][i] * self.derivada_funcion_de_activacion(self.y[i][patron])

    def calcular_error_rms(self):
        self.rms = sum(self.ep) / self.p
        self.errores.append(self.rms)
        print('RMS: %s' % self.rms)

    def graficar_errores(self):
        ylim(-1,1)
        plot(self.errores)
        show()

    def guardar_pesos_y_umbrales(self):
        pickle.dump(self.w, open('pesos.db', 'wb'))
        pickle.dump(self.u, open('umbrales.db', 'wb'))

    def entrenar(self, patrones_de_entrada, salida_deseada):
        self.x = patrones_de_entrada # x[[patron 1], [patron 2], ...]
        self.yd = salida_deseada # y[[salida 1], [salida 2], ...]
        self.establecer_cantidad_de_patrones()
        self.configurar_red_antes_de_entrenamiento()
        for iteration in range(self.trainParam['epochs']):
            for patron in range(self.p):
                self.calcular_salida_de_la_red(patron)
                self.calcular_errores_lineales_del_patron(patron)
                self.calcular_errores_no_lineales_del_patron(patron)
                self.calcular_error_del_patron(patron)
                self.actualzar_pesos_y_umbrales(patron)
            self.calcular_error_rms()
            if self.rms <= self.trainParam['goal']:
                self.graficar_errores()
                self.guardar_pesos_y_umbrales()
                break
            elif iteration >= 999:
                self.graficar_errores()

    def simular(self, inputs):
        pass

entradas = [
    [2, 2, 2, 2, 10, 10, 2, 2, 2, 2],
    [10,6, 6, 6,  6,  6, 6, 6, 6, 6],
    [10,4, 4, 4,  4,  4, 4, 4, 4, 4],
    [2, 2, 2, 5, 10, 10, 7, 8, 2, 2],
    [10, 10, 6, 4, 4,6,  6, 6, 6, 6]
]
salidas = [
    [1, 0, 0, 0, 0]
]

red = BackPropagationUnicapa(len(entradas[0]), len(salidas))
red.funcion_de_activacion = 'tanh'
red.entrenar(entradas, salidas)
